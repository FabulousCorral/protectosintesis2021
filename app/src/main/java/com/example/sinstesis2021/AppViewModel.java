package com.example.sinstesis2021;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.sinstesis2021.pojo.Tarea;


public class AppViewModel extends AndroidViewModel {
    public MutableLiveData<Tarea> selectedPost = new MutableLiveData<>();

    public AppViewModel(@NonNull Application application) {
        super(application);
    }
}
