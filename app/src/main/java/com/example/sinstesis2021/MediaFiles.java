package com.example.sinstesis2021;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;

public class MediaFiles {
    public enum Type {IMAGE, AUDIO}

    public static class UriPathFile {
        public File file;
        public Uri uri;
        public String path;

        public UriPathFile(File file, Uri uri, String path) {
            this.file = file;
            this.uri = uri;
            this.path = path;
        }
    }

    public static UriPathFile createFile(Context context, MediaFiles.Type type) {
        UriPathFile uriPathFile = null;
        try {
            String prefix = "";
            String suffix = "";
            String directory = null;
            switch (type) {
                case IMAGE:
                    prefix = "img";
                    suffix = ".jpg";
                    directory = Environment.DIRECTORY_PICTURES; // files/pictures
                    break;
                case AUDIO:
                    prefix = "aud";
                    suffix = ".3gp";
                    directory = "files/audios";
                    break;
            }
            File storageDir = context.getExternalFilesDir(directory);
            File file = File.createTempFile(prefix, suffix, storageDir);
            Uri fileUri = FileProvider.getUriForFile(context, "com.example.gerard.socialapp.fileprovider", file);

            uriPathFile = new UriPathFile(file, fileUri, file.getAbsolutePath());
        } catch (IOException e){
            Log.d("createFileError", e.getLocalizedMessage());
        }

        return uriPathFile;
    }

}
