package com.example.sinstesis2021;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.sinstesis2021.fragments.ChatFragment;
import com.example.sinstesis2021.fragments.HomeFragment;
import com.example.sinstesis2021.fragments.ProfileFragment;
import com.example.sinstesis2021.fragments.TasksFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity {


    BottomNavigationView mBottomNavigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Mostrar primer fragment por defecto
        showSelectedFragment(new HomeFragment());

        //mBottomNavigation = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        mBottomNavigation = (BottomNavigationView) findViewById(R.id.bottomNavigation);


        mBottomNavigation.setOnNavigationItemSelectedListener(menuItem -> {

            if (menuItem.getItemId() == R.id.menu_home){
                showSelectedFragment(new HomeFragment());
            }
            if (menuItem.getItemId() == R.id.menu_bidiCode){
                escanear();
            }
            if (menuItem.getItemId() == R.id.menu_tasks){
                showSelectedFragment(new TasksFragment());
            }
            if (menuItem.getItemId() == R.id.menu_chat){
                showSelectedFragment(new ChatFragment());
            }
            if (menuItem.getItemId() == R.id.menu_profile){
                showSelectedFragment(new ProfileFragment());
            }
            return true;
        });
    }

    //QR
    private void escanear() {
        new IntentIntegrator(this).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(result != null){
            if(result.getContents() == null){
                Toast.makeText(this, "Cancelaste el escaneo", Toast.LENGTH_SHORT).show();
            } else {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast, null);
                Toast toast = new Toast (getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    /*
    * MÉTODO QUE PERMITE ELEGIR EL FRAGMENT
    */
    public void showSelectedFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void miOnClick(View view) {
        Toast.makeText(this, "Bieen ", Toast.LENGTH_SHORT).show();


        Intent pasrPantalla;
        finish();
        pasrPantalla=new Intent(this,TasksFragment.class);
        startActivity(pasrPantalla);
    }

}
