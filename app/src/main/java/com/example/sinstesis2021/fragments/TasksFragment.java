package com.example.sinstesis2021.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.adapter.adapterTareas;
import com.example.sinstesis2021.pojo.Tarea;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class TasksFragment extends Fragment {

    private FloatingActionButton btnCrearTarea;

    FirebaseFirestore database;
    adapterTareas adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tasks, container, false);

        //CONEXION BBDD Y MUESTRA DE TAREAS
        database=FirebaseFirestore.getInstance();
        Task<QuerySnapshot> listaDeTareasRaw=getTareasList();
        listaDeTareasRaw.addOnCompleteListener((task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Tarea taskTemp=document.toObject(Tarea.class);
                    String author=taskTemp.getAsignadoPor();
                    Log.d("TareasTest", author);

                }
            } else {
                Log.d("TareasTest", "Error getting documents: ", task.getException());
            }
        }));
        //addNewTarea1Test(); //it works, but should be implemented better
        SearchView searchbar=view.findViewById(R.id.search2);
        //searchbar.setOnSearchClickListener(v -> buscar(searchbar.getQuery().toString(), database));
        searchbar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                buscar(query, database);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText, database);
                return false;
            }
        });

        ///////////////////////

        //BOTON CREAR TAREAS
       btnCrearTarea= (FloatingActionButton) view.findViewById(R.id.btnCrearTarea);
         btnCrearTarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"aasd",Toast.LENGTH_SHORT).show();

                Fragment fragment = new NewPostFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
                /*((MainActivity)getActivity()).showSelectedFragment(fragment);*/
            }
        });
        return view;

    }
    ///////////////////////

    private Task<QuerySnapshot> getTareasList() {
        //FirebaseFirestoreSettings set=new FirebaseFirestoreSettings();
        return database.collection("/tareas").get();
    }

    private void buscar(String s,FirebaseFirestore db) {
        db.collection("/tareas").whereArrayContains("asignado a", s).get().addOnCompleteListener(
                task -> {
                    if (task.isSuccessful()) {
                        RecyclerView rv = getActivity().findViewById(R.id.rv_tareas);

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Tarea tarea=document.toObject(Tarea.class);
                            String name=tarea.getAsignadoPor();
                            Log.d("ProductsTest", name);
                        }
                        adapter = new adapterTareas(task.getResult().toObjects(Tarea.class));
                        rv.setAdapter(adapter);
                        rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
                            /*for (QueryDocumentSnapshot document : task.getResult()) {
                                String name=document.toObject(producto.class).getNombre();
                                Log.d("ProductsTest", name);
                            }*/
                    } else {
                        Log.d("TareasTest", "Error getting documents: ", task.getException());
                    }
                    //Log.d("TaskTest",task.getResult().getDocuments().toArray().toString());

                }
        );

    }

    /////ASIGNACION NUEVAS TAREAS A JAVI, YA QUE EL SERÁ EL QUE ENSEÑE LA APLICACIÓN

    private void addNewTarea1Test(){
        Map<String,Object> newTarea1= new HashMap<>();
        ArrayList<String> asignees=new ArrayList<String>();
        asignees.add("Javier Fernández");
        newTarea1.put("asignado a", asignees);
        newTarea1.put("asignado por", "Raúl Zapatero");
        newTarea1.put("descripcion", "Debes hacer las funcionalidades de TasksFragment.java");
        newTarea1.put("estado","asignado");
        database.collection("tareas").document(Calendar.getInstance().getTime().toString()).set(newTarea1)
                .addOnCompleteListener(task -> Log.d("addTarea1", "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w("addTarea1", "Error writing document", e));
    }



}