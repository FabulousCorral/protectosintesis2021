package com.example.sinstesis2021.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.adapter.adapterUsuarios;
import com.example.sinstesis2021.pojo.Usuario;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class VerUsuariosFragment extends Fragment {

    //private RecyclerView rv_usuarios;
    //private  FirebaseRecyclerAdapter adapter;

    FirebaseFirestore database;
    adapterUsuarios adapter;

    public VerUsuariosFragment() {
        // Required empty public constructor
    }

    /*public static VerUsuariosFragment newInstance(String param1, String param2) {
        VerUsuariosFragment fragment = new VerUsuariosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this.getContext());
            rv_usuarios.setLayoutManager(linearLayoutManager);

    }
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ver_usuarios, container, false);

        database=FirebaseFirestore.getInstance();
        Task<QuerySnapshot> listaDeUsuariosRaw=getUsuariosList();
        listaDeUsuariosRaw.addOnCompleteListener((task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    String username=document.toObject(Usuario.class).getUsername();
                    Log.d("UsuariosTest", username);

                }
            } else {
                Log.d("UsuariosTest", "Error getting documents: ", task.getException());
            }
        }));

        SearchView searchbar=view.findViewById(R.id.search3);
        //searchbar.setOnSearchClickListener(v -> buscar(searchbar.getQuery().toString(), database));
        searchbar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                buscar(query, database);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText, database);
                return false;
            }
        });
         //rv_usuarios=(RecyclerView) view.findViewById(R.id.rv_usuarios);
         //FirebaseUser user = SignInActivity.getmAuth().getCurrentUser();

         //Query query = SignInActivity.getmAuth().mAuth().database.collection("Usuarios");
         // ((FirebaseAuth)(SignInActivity.getmAuth())).("Usuarios");

          //database("grupos").document("/boss").get();
         //database.collection("/grupos").document("/superadmin").get();
        //CollectionReference usersCollectionRef = SignInActivity.getmAuth().c("users");

       // CollectionReference usersCollectionRef = SignInActivity.getmAuth().("users");

      /*  FirebaseRecyclerOptions<Usuario> options= new  FirebaseRecyclerOptions.Builder<Usuario>().setQuery(query,Usuario.class).build();

         adapter = new FirebaseRecyclerAdapter<Usuario, UsuarioViewHolder>(options) {
            @Override
            public UsuarioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_usuarios, parent, false);

                return new UsuarioViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(UsuarioViewHolder holder, int position, Usuario model) {

                // Bind the Chat object to the ChatHolder
               holder.getTxtNombreUsuario().setText(model.getUsername());
               holder.getLayoutViewUsuarios().setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                     //  Toast.makeText(VerUsuariosFragment.this,"key :"+,Toast.LENGTH_SHORT.show());

                   }
               });
            }
        };*/
        //rvUsuarios.setAdapter(adapter);

        return view;

    }

    //ESTO HABRÁ QUE CAMBIARLO
    private void buscar(String s,FirebaseFirestore db) {
        db.collection("/mensaje").whereGreaterThanOrEqualTo("enviado por",s).whereLessThanOrEqualTo("enviado por",s+'\uf8ff').get().addOnCompleteListener(
                task -> {
                    if (task.isSuccessful()) {
                        RecyclerView rv = getActivity().findViewById(R.id.rv_usuarios);
                        adapter=new adapterUsuarios(task.getResult().toObjects(Usuario.class));
                        rv.setAdapter(adapter);
                        rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
                            /*for (QueryDocumentSnapshot document : task.getResult()) {
                                String name=document.toObject(producto.class).getNombre();
                                Log.d("ProductsTest", name);
                            }*/
                    } else {
                        Log.d("UsuariosTest", "Error getting documents: ", task.getException());
                    }
                    //Log.d("TaskTest",task.getResult().getDocuments().toArray().toString());

                }
        );

    }
    private Task<QuerySnapshot> getUsuariosList() {
        //FirebaseFirestoreSettings set=new FirebaseFirestoreSettings();
        return database.collection("/mensaje").get();
    }
    /*
    @Override
    public void onStart() {
        super.onStart();
       // adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        //adapter.stopListening();
    }*/

}