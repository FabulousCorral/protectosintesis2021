package com.example.sinstesis2021.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.adapter.adapterProductos;
import com.example.sinstesis2021.pojo.producto;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    PieChart pieChart;
    ////
    FirebaseFirestore database;

    ArrayList<producto> list;
    RecyclerView rv;
    SearchView searchView;
    adapterProductos adapter;

    LinearLayoutManager lm;
    ///

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        pieChart = view.findViewById(R.id.grafico);
        crearGrafico();
        database=FirebaseFirestore.getInstance();
        Task<QuerySnapshot> listaDeProductosRaw=getProductsList();
        listaDeProductosRaw.addOnCompleteListener((task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    String name=document.toObject(producto.class).getNombre();
                    Log.d("ProductsTest", name);

                }
            } else {
                Log.d("ProductsTest", "Error getting documents: ", task.getException());
            }
        }));
        //addNewShoesTest(); //it works, but should be implemented better
        SearchView searchbar=view.findViewById(R.id.search);
        //searchbar.setOnSearchClickListener(v -> buscar(searchbar.getQuery().toString(), database));
        searchbar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                buscar(query, database);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText, database);
                return false;
            }
        });
        return view;

    }

    private void buscar(String s,FirebaseFirestore db) {
        db.collection("/products").whereGreaterThanOrEqualTo("nombre",s).whereLessThanOrEqualTo("nombre",s+'\uf8ff').get().addOnCompleteListener(
                task -> {
                        if (task.isSuccessful()) {
                            RecyclerView rv = getActivity().findViewById(R.id.rv);
                            adapter=new adapterProductos(task.getResult().toObjects(producto.class));
                            rv.setAdapter(adapter);
                            rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
                            /*for (QueryDocumentSnapshot document : task.getResult()) {
                                String name=document.toObject(producto.class).getNombre();
                                Log.d("ProductsTest", name);
                            }*/
                        } else {
                            Log.d("ProductsTest", "Error getting documents: ", task.getException());
                        }
                    //Log.d("TaskTest",task.getResult().getDocuments().toArray().toString());

                }
        );

    }


    private void crearGrafico() {

        Description description = new Description();
        description.setText("");

        pieChart.setDescription(description);

        ArrayList<PieEntry> pieEntries = new ArrayList<>();

        pieEntries.add(new PieEntry(35f, 3));
        pieEntries.add(new PieEntry(65f, 7));

        PieDataSet pieDataSet = new PieDataSet(pieEntries, "Leyenda");

        int [] color = {Color.rgb(253, 225, 126), Color.rgb(0, 97, 193)};
        pieDataSet.setColors(color);

        int colorBlue = Color.parseColor("#001D86");
        pieDataSet.setValueTextColor(colorBlue);


        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private Task<QuerySnapshot> getProductsList(){
        //FirebaseFirestoreSettings set=new FirebaseFirestoreSettings();
        return database.collection("/products").get();
    }
    private void addNewShoesTest(){
        Map<String,Object> newShoes= new HashMap<>();
        newShoes.put("nombre", "zapatillas guay");
        newShoes.put("descripcion", "zapatillas tope super mega guay");
        newShoes.put("categoria", "calzado");
        newShoes.put("subcategoria", Arrays.asList("vestimenta formal"));
        newShoes.put("precio", (double)3.5);
        Map<String,Integer> location=new HashMap<>();
        location.put("columna",0); location.put("piso",0); location.put("estanteria",0); location.put("estante",0);
        newShoes.put("ubicacion", location);
        //database.collection("/products").document(Calendar.getInstance().getTime().toString()).set(newShoes)
        database.collection("products").document(Calendar.getInstance().getTime().toString()).set(newShoes)
                .addOnCompleteListener(task -> Log.d("addShoes", "DocumentSnapshot successfully written!"))
                .addOnFailureListener(e -> Log.w("addShoes", "Error writing document", e));
    }
}