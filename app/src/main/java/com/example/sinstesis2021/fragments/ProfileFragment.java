package com.example.sinstesis2021.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.SignInActivity;
import com.google.firebase.auth.FirebaseUser;



public class ProfileFragment extends Fragment implements View.OnClickListener {
    TextView nombre, correo, id;

    Button buttonLogout;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        view.findViewById(R.id.buttonLogout).setOnClickListener(v -> logOut(v));
        nombre = (TextView)view.findViewById(R.id.nombre);
        correo = (TextView)view.findViewById(R.id.correo);

        FirebaseUser user = SignInActivity.getmAuth().getCurrentUser();

        if(user != null){
            String name = user.getDisplayName();
            String email = user.getEmail();

            nombre.setText(name);
            correo.setText(email);
        }else{
            nombre.setText("No encuentra al usuario");
            correo.setText("No encuentra la cuenta");
        }
        return view;
    }


    public void logOut(View v){
        SignInActivity.getmAuth().signOut();
        Intent goodBye= new Intent(this.getContext(), SignInActivity.class);
        startActivity(goodBye);
        if(this.getActivity()!=null)
        this.getActivity().finish();
    }

    @Override
    public void onClick(View v) {
    }
}