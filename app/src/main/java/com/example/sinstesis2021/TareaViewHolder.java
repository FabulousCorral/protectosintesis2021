package com.example.sinstesis2021;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TareaViewHolder extends RecyclerView.ViewHolder{
    ImageView authorPhotoImageView;
    TextView authorTextView, contentTextView;

    TareaViewHolder(@NonNull View itemView) {
        super(itemView);

       // authorPhotoImageView = itemView.findViewById(R.id.authorPhotoImageView);
        authorTextView = itemView.findViewById(R.id.authorTextView);
        contentTextView = itemView.findViewById(R.id.contentTextView);
    }
}