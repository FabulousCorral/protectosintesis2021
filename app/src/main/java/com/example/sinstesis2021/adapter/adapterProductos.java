package com.example.sinstesis2021.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.pojo.producto;

import java.util.List;

public class adapterProductos extends RecyclerView.Adapter<adapterProductos.viewholderproductos> {

    List<producto> productoList;

    public adapterProductos(List<producto> productoList) {
        this.productoList = productoList;
    }

    @NonNull
    @Override
    public viewholderproductos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_productos,parent,false);
        viewholderproductos holder = new viewholderproductos(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull viewholderproductos holder, int position) {

        producto pr = productoList.get(position);

        holder.tv_nombre.setText(pr.getNombre());
        holder.tv_categoria.setText(pr.getCategoria());
        holder.tv_descripcion.setText(pr.getDescripcion());
        holder.tv_precio.setText(Double.toString(pr.getPrecio()));
    }

    @Override
    public int getItemCount() {
        return productoList.size();
    }

    public class viewholderproductos extends RecyclerView.ViewHolder {

        TextView tv_nombre, tv_categoria, tv_descripcion, tv_precio;

        public viewholderproductos(@NonNull View itemView) {
            super(itemView);

            tv_nombre = itemView.findViewById(R.id.tv_nombre);
            tv_categoria = itemView.findViewById(R.id.tv_categoria);
            tv_descripcion = itemView.findViewById(R.id.tv_descripcion);
            tv_precio = itemView.findViewById(R.id.tv_precio);
        }
    }
}
