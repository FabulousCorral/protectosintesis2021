package com.example.sinstesis2021.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.pojo.Usuario;

import java.util.List;

public class adapterUsuarios extends RecyclerView.Adapter<adapterUsuarios.viewholderusuarios> {

    List<Usuario> usuarioList;

    public adapterUsuarios(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @NonNull
    @Override
    public viewholderusuarios onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_usuarios,parent,false);
        viewholderusuarios holder = new viewholderusuarios(v);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull viewholderusuarios holder, int position) {

        Usuario us = usuarioList.get(position);

        holder.tv_username.setText(us.getUsername());
        holder.tv_email.setText(us.getEmail());

    }

    @Override
    public int getItemCount() {
        return usuarioList.size();
    }

    public class viewholderusuarios extends RecyclerView.ViewHolder {

        TextView tv_username, tv_email;

        public viewholderusuarios(@NonNull View itemView) {
            super(itemView);

            tv_username = itemView.findViewById(R.id.tv_username);
            tv_email = itemView.findViewById(R.id.tv_email);
        }
    }
}
