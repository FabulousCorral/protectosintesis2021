package com.example.sinstesis2021.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sinstesis2021.R;
import com.example.sinstesis2021.pojo.Tarea;

import java.util.List;

public class adapterTareas extends RecyclerView.Adapter<adapterTareas.viewholdertareas> {

    List<Tarea> tareaList;

    public adapterTareas(List<Tarea> tareaList) {
        this.tareaList = tareaList;
    }

    @NonNull
    @Override
    public viewholdertareas onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tareas,parent,false);
        viewholdertareas holder = new viewholdertareas(v);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull viewholdertareas holder, int position) {

        Tarea tr = tareaList.get(position);

        holder.tv_author.setText(tr.getAsignadoPor());
        holder.tv_content.setText(tr.getDescripcion());
        holder.tv_remitente.setText(tr.getAsignadoA().get(0));//temp fix because aaaaaaaaaaaaaaaaaaaaaaaaaaaaaah

    }

    @Override
    public int getItemCount() {
        return tareaList.size();
    }

    public class viewholdertareas extends RecyclerView.ViewHolder {

        TextView tv_author, tv_content, tv_remitente;

        public viewholdertareas(@NonNull View itemView) {
            super(itemView);

            tv_author = itemView.findViewById(R.id.tv_author);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_remitente = itemView.findViewById(R.id.tv_remitente);
        }
    }
}
