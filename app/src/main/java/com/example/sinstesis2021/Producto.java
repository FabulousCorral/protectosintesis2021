package com.example.sinstesis2021;

import java.util.HashMap;
import java.util.Map;

public class Producto {
    public String id;
    public String ubicacion;
    public String descripcion;
    public int cantidad;
    public int precio;

    public String nombre;

    public int starCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    public Producto(){

    }
    public Producto( String id,String ubicacion, String descripcion, int cantidad,int precio,String nombre){

        this.id = id;
        this.ubicacion = ubicacion;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.nombre = nombre;
    }
    public  Map <String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("ubicacion", ubicacion);
        result.put("descripcion", descripcion);
        result.put("cantidad", cantidad);
        result.put("precio", precio);
        result.put("nombre", nombre);

        return result;
    }
}
