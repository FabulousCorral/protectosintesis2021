package com.example.sinstesis2021.pojo;

import com.google.firebase.firestore.PropertyName;

import java.util.ArrayList;

public class Tarea {

    @PropertyName("asignado a")
    public ArrayList<String> asignadoA;
    @PropertyName("asignado por")
    public String asignadoPor;
    public String descripcion;
    public String estado;


    public Tarea() {}

    public Tarea(ArrayList<String> asignadoA, String author, String descripcion, String estado) {
        this.asignadoA = asignadoA;
        this.asignadoPor = author;
        this.descripcion = descripcion;
        this.estado = estado;

    }

    public ArrayList<String> getAsignadoA() {
        return asignadoA;
    }

    public void setAsignadoA(ArrayList<String> asignadoA) {
        this.asignadoA = asignadoA;
    }

    public String getAsignadoPor() {
        return asignadoPor;
    }

    public void setAsignadoPor(String asignadoPor) {
        this.asignadoPor = asignadoPor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}

