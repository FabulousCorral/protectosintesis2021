package com.example.sinstesis2021;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class UsuarioViewHolder extends RecyclerView.ViewHolder {

    private LinearLayout layoutViewUsuarios ;
    public TextView getTxtNombreUsuario() {
        return txtNombreUsuario;
    }

    public void setTxtNombreUsuario(TextView txtNombreUsuario) {
        this.txtNombreUsuario = txtNombreUsuario;
    }

    private TextView txtNombreUsuario;
    public UsuarioViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
        layoutViewUsuarios=itemView.findViewById(R.id.layoutViewUsuarios);

        txtNombreUsuario=itemView.findViewById(R.id.txtNombreUsuario);
    }

    public LinearLayout getLayoutViewUsuarios() {
        return layoutViewUsuarios;
    }

    public void setLayoutViewUsuarios(LinearLayout layoutViewUsuarios) {
        this.layoutViewUsuarios = layoutViewUsuarios;
    }
}
