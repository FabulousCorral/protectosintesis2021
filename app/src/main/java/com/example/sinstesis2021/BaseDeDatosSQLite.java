package com.example.sinstesis2021;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BaseDeDatosSQLite extends SQLiteOpenHelper {

    protected  SQLiteDatabase db;

    public BaseDeDatosSQLite(@Nullable Context context  ) {
        super(context, "tareas", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table tareas(id integer primary key autoincrement not null,nombre text,accion text,autor text,prority int,estado boolean )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists tareas");

    }

    public  void insertarTarea( String nombre,String accion,String autor,int priority,boolean estado){

        db=this.getReadableDatabase();
        db.execSQL("insert into tareas (nombre ,accion /*,autor*/ ,prority,estado ) values ('"+nombre+"',"+accion+","+autor+","+priority+","+estado+")");


    }


    public void updateTarea(int id, String nombre, int priority){
        db=this.getWritableDatabase();
        db.execSQL("update tareas set nombre='"+nombre+"',priority="+priority+" where id="+id);

    }

    public int NumTareas(){
        int num=0;
        db=this.getReadableDatabase();
        num=(int) DatabaseUtils.queryNumEntries(db,"tareas");
        return num;


    }
    public void borrarTareas(){
        db=this.getWritableDatabase();
        db.execSQL("delete from tareas");

    }
    public void borrarTarea(int id){
        db=this.getWritableDatabase();
        db.execSQL("delete from tareas where id="+id);

    }

    public ArrayList<String> getAllTareas(){

        ArrayList <String> filas=new ArrayList<String>();
        Cursor res=null;
        String contenido="";
        if (NumTareas() >0){
            db=this.getWritableDatabase();
            res=db.rawQuery("select * from tareas order by priority asc",null);
            res.moveToFirst();
            while (res.isAfterLast()==false){
                contenido=res.getInt(res.getColumnIndex("id"))+".-"+res.getString(res.getColumnIndex("nombre"));
                filas.add(contenido);
                res.moveToNext();

            }
        }

        return filas;

    }

  /*  public Tarea getTarea(int id){

        Tarea n=null;
        Cursor res=null;
        String contenido="";
        if (NumTareas()>0){
            db=this.getReadableDatabase();
            res=db.rawQuery("select * from notas where id="+id+" order by priority asc",null);
            res.moveToFirst();
            while (res.isAfterLast()==false){
                n = new Tarea(res.getInt(res.getColumnIndex("id")),res.getString(res.getColumnIndex("nombre")),res.getInt(res.getColumnIndex("priority")));
                res.moveToNext();
            }
        }
        return n;
    }

*/
}
